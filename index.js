const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const os = require('os');
const cors = require('cors');
require('dotenv').config();

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(express.static(`${os.tmpdir()}/public`));
// app.use(cors({ origin: require('./environment/').ao }));

const api = require('./config');
app.use('/api', api);

const server = http.Server(app);

/**
 * @description Local server listen.
 */
server.listen(process.env.PORT || 5000);