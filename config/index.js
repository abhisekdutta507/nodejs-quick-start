const express = require('express');

const Test = require('../api/Test/controller');

const api = express.Router();

/**
 * @description version 1 API routes
 */
const routes = () => {
  api.get('/test', Test.find);

  return api;
};

module.exports = routes();