This project was created using `npm init`. It is a simple Node JS poject created using Express.

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:
```bash
npm start
```

Runs the app in the development mode.

Open [http://localhost:5000/api/test](http://localhost:5000/api/test) to view it in the browser.

## Learn More

You can learn more in the [Node JS documentation](https://nodejs.org/en/docs/).

To learn Express, check out the [Express 4.x documentation](https://expressjs.com/en/4x/api.html).

### Install Node JS in your machine to start the setup.

### Simple Node JS Application Setup With Express

**Init project using `npm init` command.**
```bash
npm init
```

Type in the project details & press **y** to proceed. It creates a package.json file for the project.

**Add the required dependencies.**

```bash
npm install express dotenv body-parser cors nodemon
```

**Manually create the `.env` file in your project root.**

Add the following codes:
```env
# Server Application Configuration
PORT=5000

# Set Server Type
DEVELOPMENT=true
```

**Let's create the 1st API.**

Create `api` folder in your project root.

Inside the `api` folder create a `Test` folder.

Then, create `controller.js` inside the `Test` folder.

Now add the following codes in `./api/Test/controller.js` file.

```js
const Controller = {

  find: async (req, res) => {
    // setTimeout(() => { return res.status(200).send({ message: 'You are connected to Node.js server.' }); }, 10000);
    return res.status(200).send({ message: 'You are connected to Node.js server.' });
  }

};

module.exports = Controller;
```

**Manually create the `config` folder in the project root. The config contains the API routings.**

**Create `index.js` in the `config` folder. Now add the first API route.**

Add the following codes:
```js
const express = require('express');

const Test = require('../api/Test/controller');

const api = express.Router();

/**
 * @description version 1 API routes
 */
const routes = () => {
  api.get('/test', Test.find);

  return api;
};

module.exports = routes();
```

**Manually create the entry point file `index.js` in your project root.** *Note: this file name might be different if you have renamed it while creating the `package.json`. Please check the entry point / main in `package.json`.*

Add the following codes:
```js
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');
require('dotenv').config();

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(express.static(__dirname + '/public'));
// app.use(cors({ origin: require('./environment/').ao }));

const api = require('./config');
app.use('/api', api);

const server = http.Server(app);

/**
 * @description Local server listen.
 */
server.listen(process.env.PORT || 5000);
```

**Override the `package.json` file.**

Replace the following scripts
```json
{
  ...,

  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },

  ...
}
```
with
```json
{
  ...,

  "scripts": {  
    "start": "nodemon index.js"
  },

  ...
}
```

**Hurray!! we are ready to go now.**

Run the app in the development mode.

```bash
npm start
```

Open [http://localhost:5000/api/test](http://localhost:5000/api/test) to view it in the browser.

### Deployment

View Heroku documentation: https://devcenter.heroku.com/articles/getting-started-with-nodejs

### Node.js interview questions

1. What is Node?
Answer: It is a runtime environment for executing JavaScript codes. Google's V8 engine was embedded inside C++ programs to create Node. It's not a programming language or neither a framework.

We can work with file systems and networks using Node. Those features are implemented using C++.

We can create highly-scalable, data-intensive and real-time applications using Node. Advantages of Node are, it is non-blocking or asynchronous in nature.

2. What is the job of an Event loop?
Answer: Event loops job is to look at the stack and the task queue / event queue. If the stack is empty it takes the first task from the queue and pushes it into the stack. Learn more [here](https://www.youtube.com/watch?v=8aGhZQkoFbQ at 14:00 minutes).

console.log('Hello World');

// WebAPI or C++ API
setTimeout(() => {
    console.log('This is Abhisek');
}, 1000);

console.log('Hello There!!');

- - - Event Stack - - -

1. console.log('This is Abhisek');

- - - Web Worker / C++ API - - -

1. 

- - - Event Queue ( FIFO ) - - -

1. 


3. Is Node JS single threaded?
Answer: Node.js is created with a C++ wrapper around Chrome's V8 engine. All our synchronous JavaScript codes, V8 engine and the special mechanism called Event loop runs in one thread which is also known as main thread. There are some WebAPIs (browser) or C++ APIs (Node.js) running behind this concept. Those are usefull in case of asynchronous operations. Node.js uses a pre allocated set of threads called the Tread pool. By default, it is 4. Learn more [here](https://www.youtube.com/watch?v=zphcsoSJMvM).

- > 1. synchronous 2. asynchronous


4. What is callback?
Answer: A callback is a function that we can pass into another function as an argument. We can invoke the callback inside the other function to complete some kind of action.

5. What is error first in callback?
Answer: The error-first callback is a function that is used to pass error and data. The first argument of this function is an error object and the other arguments represent data.

var post = new Post({title: 'test'});
post.save(function(err,savedInstance){
  if(err){
     //handle error and return
  }
  //go ahead with `savedInstance`
});

6. What is promise?
Answer: A promise is a JavaScript object that can be used to perform some asynchronous operations and get their responses through the then or catch prototypes also we can use then using async await.

7. Whay do we use promise instead of callback?
Answer: We can use promise in place of callback to achieve more clean way of running asynchronous codes.

function getNamePromise () {
    return new Promise(function(next, error) {
        setTimeout(function() {
            next({ fullName: 'Abhisek Dutta' });
        }, 2000);
    });
}

getNamePromise().then(function(param) {
    console.log(param);
});

function getName (cb) {
    setTimeout(function() {
        cb({ fullName: 'Abhisek Dutta' });
    }, 2000);
}

function responseCallback (param) {
    console.log(param);    
}

getName(responseCallback);

8. What is Async-Await?
Answer: We can get rid of function chaining with promises. It takes you 1 level up. We can write the codes with some more clean approach.

function getNamePromise () {
    return new Promise(function(next, error) {
        setTimeout(function() {
            next({ fullName: 'Abhisek Dutta' });
        }, 2000);
    });
}

async function work() {
    var user = await getNamePromise();
    console.log(user);
}

9. What is npm in Node.js?
Answer: It is a package manager for Node.js. It contains some public and private packages online that we can use in our projects. It gives us a powerful CLI. We can run commands to manage our required packages throughout the application.

10. What is module.exports?
Answer: module.exports can be used to allow some piece of code written in one file to be accessed by another.

11. What is TypeScript?
Answer: TypeScript is an open-source programming language developed and maintained by Microsoft. It is a strict syntactical superset of JavaScript.

12. JSLint vs ESLint?
Answer: Those are tools that we can use in our projects. We can use them to maintain some standared coding patterns.

13. What are Mocha, Chai and Sinon?
Answer: https://www.youtube.com/watch?v=9Orwhw1pGec

14. setTimeout() vs process.netTick() vs setTimeout()?
Answer: https://www.youtube.com/watch?v=yMIn6yonzEI

15. What is Agile?
Answer: https://www.youtube.com/watch?v=NpCEjtKAa20

Sections: Kanban, [Scrum](https://www.youtube.com/watch?v=oTZd2vo3FQU), XP (Extreme Programming)
